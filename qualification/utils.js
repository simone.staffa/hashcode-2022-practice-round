const fs = require('fs');
const readline = require('readline');
const {results} = require("./b");

async function readAndParseFile(path) {
    const fileStream = fs.createReadStream(path);

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    const result = {
        numContributors: 0,
        numProjects: 0,
        contributors: {},
        projects: {},
        skills: []
    };
    let lineNumber = 0;


    let contributors = [];
    let projects = [];
    let readSkills = -1;
    let currentContributor;
    let currentProject;
    let contributorsEnded = false;
    for await (const line of rl) {
        const content = line.split(' ');
        if (lineNumber === 0) {
            result.numContributors = content[0]
            result.numProjects = content[1]
            lineNumber++;
            continue;
        }
        if (content.length === 5) {
            contributorsEnded = true;
        }
        if (contributors.length <= result.numContributors && !contributorsEnded) { // contributors
            if (readSkills <= 0) {
                contributors.push(content[0]);
                result.contributors[content[0]] = {
                    numSkills: content[1],
                    skills: []
                };
                currentContributor = content[0]
                readSkills = parseInt(content[1]);
            } else {
                readSkills--;
                const skill = {
                    name: content[0],
                    level: content[1]
                }
                result.skills.push(skill.name + '|' + skill.level);
                result.contributors[currentContributor].skills.push(skill)
            }
        } else { // parsing projects
            if (content.length === 5) {
                projects.push(content[0]);
                result.projects[content[0]] = {
                    days: content[1],
                    score: content[2],
                    deadline: content[3],
                    numRoles: content[4],
                    sortScore: content[2] - content[1] - content[4] * 6500,
                    skills: []
                };
                currentProject = content[0]
            } else {
                const skill = {
                    name: content[0],
                    level: content[1]
                }
                result.projects[currentProject].skills.push(skill)
            }
        }
        lineNumber++;
    }

    return result;
}

async function writeFile(path, numProjects, projects) {
    const rows = [numProjects]
    for (const project of projects) {
        rows.push(project.project);
        rows.push(project.people.join(' '));
    }
    await fs.writeFileSync(path, rows.join('\n'))
}

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
process.stdin.on('keypress', (str, key) => {
    if (key.ctrl && key.name === 'c') {
        process.exit();
    }
    if (key.name === 'p') {
        console.log('sssss')
        writeFile(`qualification/output-data/${process.argv[2]}.out.txt`, results.length, results);
    }
});

module.exports = {
    readAndParseFile,
    writeFile
}