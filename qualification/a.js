const {readAndParseFile, writeFile, calcScore} = require("./utils");

const fileName = 'a_an_example';


readAndParseFile(`qualification/input-data/${fileName}.in.txt`).then( async (data) => {
    const contributorPerSkills = {};
    for (const skill of data.skills) {
        contributorPerSkills[skill] = {};
        for (const contributorKey of Object.keys(data.contributors)) {
            const contributor = data.contributors[contributorKey];
            if (contributor.skills.map(s => s.name + '-' + s.level).includes(skill)) {
                contributorPerSkills[skill][contributorKey] = 1
            } else {
                contributorPerSkills[skill][contributorKey] = 0
            }
        }
    }
    const sortedProjects = sortProjects(data.projects);
    const projectKeys = Object.keys(sortedProjects);

    const day = 0;

    const result = [];
    const people = new Set([...Object.keys(data.contributors)]);

    const currentDay = 0;

    const currentWorkingProject = {};

    projectKeys.forEach((key) => {
        const selectedProject = sortedProjects[key];

        const skillKeys = selectedProject.skills.map(s => s.name + '-' + s.level);

        const {res, selectedPeople} = getPeopleBySkill(skillKeys, people, contributorPerSkills);

        if (res) {
            result.push({
                project: key,
                people: selectedPeople
            })
            if (currentWorkingProject[currentDay + selectedProject.days]) {
                currentWorkingProject[currentDay + selectedProject.days] = [...currentWorkingProject[currentDay + selectedProject.days], ...selectedPeople]
            } else {
                currentWorkingProject[currentDay + selectedProject.days] = selectedPeople
            }
        }
    })
    await writeFile(`qualification/output-data/${fileName}.out.txt`, result.length, result)
});

function getPeopleBySkill(skillKeys, people, contributorPerSkills) {
    const selectedPeople = [];
    for (const skillKey of skillKeys) {
        const name = skillKey.split('-')[0];
        const level = skillKey.split('-')[1];
        let found = false;
        for (let i = level - 1; i <= 10; i++) {
            const k = name + '-' + i;
            if (contributorPerSkills[name + '-' + i]) {
                const objPeople = contributorPerSkills[k];
                selectedPeople.push(Object.keys(objPeople).find(p => objPeople[p] === 1));
                found = true;
                break;
            }
        }
        if (!found) {
            return {
                res: false,
                selectedPeople: []
            }
        }

    }
    for (const p of selectedPeople) {
        people.delete(p)
    }

    return {
        res: true,
        selectedPeople
    };
}

function sortProjects(projects) {
    const sortable = [];
    for (const project in projects) {
        sortable.push([project, projects[project]]);
    }

    sortable.sort((a, b) => {
        const projectA = a[1];
        const projectB = b[1];
        if (a.deadline === b.deadline) {
            return (projectA.score - projectA.days - projectA.skills.length) - (projectB.score - projectB.days - projectB.skills.length)
        } else {
            return projectB.deadline - projectA.deadline;
        }
    });
    projects = {};
    sortable.forEach(function(item){
        projects[item[0]] = item[1];
    })
    return projects;
}