const {readAndParseFile, writeFile, calcScore} = require("./utils");

const fileName = 'b_better_start_small';

const results = [];

readAndParseFile(`qualification/input-data/${fileName}.in.txt`).then( async (data) => {
    const contributorPerSkills = {};
    for (const skill of data.skills) {
        contributorPerSkills[skill] = {};
        for (const contributorKey of Object.keys(data.contributors)) {
            const contributor = data.contributors[contributorKey];
            if (contributor.skills.map(s => s.name + '|' + s.level).includes(skill)) {
                contributorPerSkills[skill][contributorKey] = 1
            } else {
                contributorPerSkills[skill][contributorKey] = 0
            }
        }
    }
    const sortedProjects = sortProjects(data.projects);
    const projectKeys = Object.keys(sortedProjects);

    const day = 0;


    const people = new Set([...Object.keys(data.contributors)]);

    let currentDay = 0;

    const peopleCurrentlyWorking = {};


    const lastingProjects = new Set([...projectKeys]);

    while (lastingProjects.size > 0) {
        lastingProjects.forEach((key) => {
            const selectedProject = sortedProjects[key];

            if (currentDay > selectedProject.deadline) {
                lastingProjects.delete(key);
            }

            const skillKeys = selectedProject.skills.map(s => s.name + '|' + s.level);

            const {res, selectedPeople} = getPeopleBySkill(skillKeys, people, contributorPerSkills);

            if (res) {
                results.push({
                    project: key,
                    people: selectedPeople.map(p => p.person)
                })
                if (peopleCurrentlyWorking[currentDay + parseInt(selectedProject.days, 10)]) {
                    peopleCurrentlyWorking[currentDay + parseInt(selectedProject.days, 10)] = [...peopleCurrentlyWorking[currentDay + parseInt(selectedProject.days, 10)], ...selectedPeople]
                } else {
                    peopleCurrentlyWorking[currentDay + parseInt(selectedProject.days, 10)] = selectedPeople
                }
                lastingProjects.delete(key);
            } else {
                const currentWorkingProjectDays = Object.keys(peopleCurrentlyWorking).sort((a, b) => {
                    return a.localeCompare(b);
                });
                if (currentWorkingProjectDays.length >= 1) {
                    const finishedProjectsPeople = peopleCurrentlyWorking[currentWorkingProjectDays[0]];
                    for (const p of finishedProjectsPeople) {
                        people.add(p.person);
                    }
                    delete peopleCurrentlyWorking[currentWorkingProjectDays[0]]
                    currentDay += parseInt(currentWorkingProjectDays[0])
                }

            }
        })
    }


    //console.log(results);
    await writeFile(`qualification/output-data/${fileName}.out.txt`, results.length, results)
});

function getPeopleBySkill(skillKeys, people, contributorPerSkills) {
    const selectedPeople = [];
    const copyContributorPerSkills ={
        ...contributorPerSkills
    }
    for (const skillKey of skillKeys) {
        const name = skillKey.split('|')[0];
        const level = skillKey.split('|')[1];
        let found = false;
        for (let i = 10; i >= level; i--) {
            const k = name + '|' + i;
            if (copyContributorPerSkills[k]) {
                const objPeople = copyContributorPerSkills[k];
                const person = Object.keys(objPeople).find(p => objPeople[p] === 1 && !selectedPeople.includes(p));
                if (person) {
                    delete copyContributorPerSkills[k][person]
                    selectedPeople.push({person, skill: {name, level: i}});
                    found = true;
                    const newLevel = i + 1;
                    if (newLevel < 10 && level >= i) {
                        if (!contributorPerSkills[name + '|' + (newLevel)]) {
                            contributorPerSkills[name + '|' + (newLevel)] = {}
                        }
                        contributorPerSkills[name + '|' + (newLevel)][person] = 1
                    }
                    break;
                }
            }
        }
        if (!found) {
            return {
                res: false,
                selectedPeople: []
            }
        }

    }
    for (const p of selectedPeople) {
        people.delete(p.person)
    }

    return {
        res: true,
        selectedPeople
    };
}

function sortProjects(projects) {
    const sortable = [];
    for (const project in projects) {
        sortable.push([project, projects[project]]);
    }

    sortable.sort((a, b) => {
        const projectA = a[1];
        const projectB = b[1];
        if (a.deadline === b.deadline) {
            return projectB.sortScore - projectA.sortScore
        } else {
            return projectA.deadline - projectB.deadline;
        }
    });
    projects = {};
    sortable.forEach(function(item){
        projects[item[0]] = item[1];
    })
    return projects;
}

module.exports = {
    results
}