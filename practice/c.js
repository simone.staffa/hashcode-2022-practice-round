const {readAndParseFile, writeFile, calcScore} = require("./utils");

const fileName = 'c_coarse'

readAndParseFile(`input_data/${fileName}.in.txt`).then(async (data) => {
    const results = [];
    for (const ingredient of Object.keys(data.ingredients)) {
        if (data.ingredients[ingredient].like - data.ingredients[ingredient].dislike > 0) {
            results.push(ingredient)
        }
    }
    await writeFile(`output_data/${fileName}.out.txt`, results);
    const score = await calcScore(`output_data/${fileName}.out.txt`, data);
    console.log('SCORE: ' + score);
})