const {readAndParseFile, writeFile, calcScore} = require("./utils");

const fileName = 'd_difficult';

readAndParseFile(`input_data/${fileName}.in.txt`).then(async (data) => {
    const {clients} = data;

    const ingredients = new Set();

    Object.values(data.ingredients).forEach((ing, index) => {
        if (ing.dislike <= 0) {
            ingredients.add(Object.keys(data.ingredients)[index]);
        }
    })

    const negativeIngredients = new Set();
    for (const client of clients) {
        let jump = false;

        client.dislike.forEach(ing => {
            if (ingredients.has(ing)) {jump = true}
            if (negativeIngredients.has(ing)) {jump = true}
        })

        if (jump) continue;

        client.like.forEach(el => ingredients.add(el));
        client.dislike.forEach(el => negativeIngredients.add(el));
    }
    
    await writeFile(`output_data/${fileName}-tempt1.out.txt`, [...ingredients]);
    const score = await calcScore(`output_data/${fileName}-tempt1.out.txt`, data);
    console.log('SCORE: ' + score);
})