const {calcScore} = require("./utils");

const path = require('path');


calcScore(`${__dirname}/input_data/a_an_example.in.txt`, `${__dirname}/output_data/output.out.txt`).then((score) => {
    console.log('Your score: ' + score)
    console.log('DONE!!')
})