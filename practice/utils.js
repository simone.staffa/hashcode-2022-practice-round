const fs = require('fs');
const readline = require('readline');

async function readAndParseFile(path) {
    const fileStream = fs.createReadStream(path);

    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    const result = {
        numClients: 0,
        clients: [],
        ingredients: {}
    };
    let lineNumber = 0;
    for await (const line of rl) {
        if (lineNumber === 0) {
            result.numClients = line;
            lineNumber++;
            continue;
        }
        const data = line.split(' ');
        const numIngredients = data[0];
        const ingredients = data.slice(1);
        if (lineNumber % 2 !== 0) { // LIKE
            result.clients.push({
                like: ingredients,
                dislike: [],
                tot: ingredients.length
            })
            for (const ingredient of ingredients) {
                if (!result.ingredients[ingredient]) {
                    result.ingredients[ingredient] = {
                        like: 0,
                        dislike: 0
                    }
                }
                result.ingredients[ingredient].like++;
            }
        }
        if (lineNumber % 2 === 0) { // DISLIKE
            result.clients[result.clients.length -1].dislike = ingredients;
            result.clients[result.clients.length -1].tot = result.clients[result.clients.length -1].tot + ingredients.length;
            for (const ingredient of ingredients) {
                if (!result.ingredients[ingredient]) {
                    result.ingredients[ingredient] = {
                        like: 0,
                        dislike: 0
                    }
                }
                result.ingredients[ingredient].dislike++;
            }
        }
        lineNumber++;
    }
    const sortable = [];
    for (const ingredient in result.ingredients) {
        sortable.push([ingredient, result.ingredients[ingredient]]);
    }

    sortable.sort((a, b) => {
        return (b[1].like - b[1].dislike) - (a[1].like - a[1].dislike);
    });
    result.ingredients = {};
    sortable.forEach(function(item){
        result.ingredients[item[0]] = item[1];
    })

    const totIng = Object.keys(result.ingredients).length;
    console.log(totIng);

    result.clients.sort((a, b) => {
        let apoints = 0;
        let bpoints = 0;
        
        a.like.forEach((el) => {
            apoints = apoints + result.ingredients[el].like;
        })

        a.dislike.forEach((el) => {
            apoints = apoints - result.ingredients[el].dislike - 20;
        })

        b.like.forEach((el) => {
            bpoints = bpoints + result.ingredients[el].like;
        })

        b.dislike.forEach((el) => {
            bpoints = bpoints - result.ingredients[el].dislike - 20;
        })

        return bpoints - apoints;
    });
    return result;
}

async function writeFile(path, ingredients) {
    const data = [ingredients.length].concat(ingredients)
    await fs.writeFileSync(path, data.join(' '))
}

async function calcScore(answerPath, result, questionPath) {
    const answerStream = fs.createReadStream(answerPath);

    const answerRL = readline.createInterface({
        input: answerStream,
        crlfDelay: Infinity
    });

    let myPizzaIngredients = [];
    for await (const line of answerRL) {
        myPizzaIngredients = line.split(' ').slice(1);
    }

    const {clients} = result || await readAndParseFile(questionPath);

    let score = 0;
    for (const client of clients) {
        const {like, dislike} = client;

        let jump = false;
        like.forEach(ing => {
            if(!myPizzaIngredients.includes(ing)) {jump = true}
        })

        dislike.forEach(ing => {
            if (myPizzaIngredients.includes(ing)) {jump = true}
        })

        if (jump) continue;
        score = score + 1;
    }
    return score;
}

module.exports = {
    readAndParseFile,
    writeFile,
    calcScore
}